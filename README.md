Flutter SDK Image
===============

## Overview ##

_Docker_ images that contain tools and components required for **building** of _Flutter_ projects.

## Tags ##

Below are listed tags for which are **available** their corresponding **images**:

- **[sdk](https://bitbucket.org/docker-universum/flutter/wiki/SDK)**
- **[api-xx](https://bitbucket.org/docker-universum/flutter/wiki/API)**

> For example image which provides configuration for **Flutter** with **Android API 28** may be used as: `image: universumstudios/flutter:api-28`.

## Cloud ##

Images are available via **[Docker Cloud](https://cloud.docker.com/swarm/universumstudios/repository/docker/universumstudios/flutter/general)**. 