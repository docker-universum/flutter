Flutter SDK Image
===============

## Overview ##

_Docker_ images that contain tools and components required for **building** of _Flutter_ projects.

## Tags ##

Below are listed tags for which are **available** their corresponding **images**:

- **[sdk](https://gitlab.com/docker-universum/flutter/wikis/SDK)**
- **[api-xx](https://gitlab.com/docker-universum/flutter/wikis/API)**

> For example image which provides configuration for **Flutter** with **Android API 28** may be used as: `image: universumstudios/flutter:api-28`.

## Cloud ##

Images are available via **[Docker Cloud](https://cloud.docker.com/swarm/universumstudios/repository/docker/universumstudios/flutter/general)**.

## [License](https://gitlab.com/docker-universum/flutter/blob/master/LICENSE.md) ##

**Copyright 2018 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.