FROM universumstudios/android:sdk

### CONFIGURATION ==================================================================================

# Environment variables:
ENV FLUTTER_HOME="/flutter/sdk"

# Installation arguments:
ARG FLUTTER_SDK_URL="https://github.com/flutter/flutter.git"

### IMAGE DEPENDENCIES & TOOLS =====================================================================

#RUN apt-get -q update && apt-get -q install -y \ 
#    lib32stdc++6 \
#    lib32z1 \
#    && apt-get -q clean && rm -rf /var/lib/apt/lists /var/cache/apt
	
### FLUTTER ========================================================================================

# Download SDK tools:
RUN set -o errexit -o nounset \
    && echo "Creating Flutter HOME directory" \
    && mkdir -p $FLUTTER_HOME \
    \
    && echo "Downloading Flutter SDK tools" \
    && git clone --branch beta $FLUTTER_SDK_URL $FLUTTER_HOME

# Export environment variables for Flutter tools specific directories.
ENV PATH="$PATH:${FLUTTER_HOME}/bin:${FLUTTER_HOME}/bin/cache/dart-sdk/bin"

# Update SDK components:
RUN set -o errexit -o nounset \
    && echo "Updating Flutter SDK components"
	# NOTE: Do not upgrade as it requires a lot of memory which is not available on the Bitbucket's CI server due to its memory limits.
    # && flutter upgrade

# Set working directory:
WORKDIR /flutter

### VERIFICATION ===================================================================================

RUN set -o errexit -o nounset \
    && echo "Testing Image tools installation" \
    && git --version \
    \
    && echo "Testing Flutter SDK configuration" \
    && flutter doctor
